﻿namespace Keliling_dan_Luas_Bangun
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbLingkaran = new System.Windows.Forms.RadioButton();
            this.rbSegilima = new System.Windows.Forms.RadioButton();
            this.rbSegitiga = new System.Windows.Forms.RadioButton();
            this.rbPersegiPanjang = new System.Windows.Forms.RadioButton();
            this.rbPersegi = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.btnKeluar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbLingkaran);
            this.groupBox1.Controls.Add(this.rbSegilima);
            this.groupBox1.Controls.Add(this.rbSegitiga);
            this.groupBox1.Controls.Add(this.rbPersegiPanjang);
            this.groupBox1.Controls.Add(this.rbPersegi);
            this.groupBox1.Location = new System.Drawing.Point(12, 65);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(283, 182);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pilih Bangun";
            // 
            // rbLingkaran
            // 
            this.rbLingkaran.AutoSize = true;
            this.rbLingkaran.Location = new System.Drawing.Point(19, 159);
            this.rbLingkaran.Name = "rbLingkaran";
            this.rbLingkaran.Size = new System.Drawing.Size(72, 17);
            this.rbLingkaran.TabIndex = 4;
            this.rbLingkaran.TabStop = true;
            this.rbLingkaran.Text = "Lingkaran";
            this.rbLingkaran.UseVisualStyleBackColor = true;
            // 
            // rbSegilima
            // 
            this.rbSegilima.AutoSize = true;
            this.rbSegilima.Location = new System.Drawing.Point(19, 125);
            this.rbSegilima.Name = "rbSegilima";
            this.rbSegilima.Size = new System.Drawing.Size(64, 17);
            this.rbSegilima.TabIndex = 3;
            this.rbSegilima.TabStop = true;
            this.rbSegilima.Text = "Segilima";
            this.rbSegilima.UseVisualStyleBackColor = true;
            // 
            // rbSegitiga
            // 
            this.rbSegitiga.AutoSize = true;
            this.rbSegitiga.Location = new System.Drawing.Point(19, 88);
            this.rbSegitiga.Name = "rbSegitiga";
            this.rbSegitiga.Size = new System.Drawing.Size(63, 17);
            this.rbSegitiga.TabIndex = 2;
            this.rbSegitiga.TabStop = true;
            this.rbSegitiga.Text = "Segitiga";
            this.rbSegitiga.UseVisualStyleBackColor = true;
            // 
            // rbPersegiPanjang
            // 
            this.rbPersegiPanjang.AutoSize = true;
            this.rbPersegiPanjang.Location = new System.Drawing.Point(19, 53);
            this.rbPersegiPanjang.Name = "rbPersegiPanjang";
            this.rbPersegiPanjang.Size = new System.Drawing.Size(102, 17);
            this.rbPersegiPanjang.TabIndex = 1;
            this.rbPersegiPanjang.TabStop = true;
            this.rbPersegiPanjang.Text = "Persegi Panjang";
            this.rbPersegiPanjang.UseVisualStyleBackColor = true;
            // 
            // rbPersegi
            // 
            this.rbPersegi.AutoSize = true;
            this.rbPersegi.Location = new System.Drawing.Point(19, 19);
            this.rbPersegi.Name = "rbPersegi";
            this.rbPersegi.Size = new System.Drawing.Size(60, 17);
            this.rbPersegi.TabIndex = 0;
            this.rbPersegi.TabStop = true;
            this.rbPersegi.Text = "Persegi";
            this.rbPersegi.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(129, 253);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnKeluar
            // 
            this.btnKeluar.Location = new System.Drawing.Point(220, 253);
            this.btnKeluar.Name = "btnKeluar";
            this.btnKeluar.Size = new System.Drawing.Size(75, 23);
            this.btnKeluar.TabIndex = 3;
            this.btnKeluar.Text = "KELUAR";
            this.btnKeluar.UseVisualStyleBackColor = true;
            this.btnKeluar.Click += new System.EventHandler(this.btnKeluar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(307, 328);
            this.Controls.Add(this.btnKeluar);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Program Menghitung Luas dan keliling";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbLingkaran;
        private System.Windows.Forms.RadioButton rbSegilima;
        private System.Windows.Forms.RadioButton rbSegitiga;
        private System.Windows.Forms.RadioButton rbPersegiPanjang;
        private System.Windows.Forms.RadioButton rbPersegi;

        private System.Windows.Forms.Button button1;

        private System.Windows.Forms.Button btnKeluar;

    }
}

