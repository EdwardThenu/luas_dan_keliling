﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Keliling_dan_Luas_Bangun
{
    public partial class Persegi : Form
    {
        public Persegi()
        {
            InitializeComponent();
        }

        double sisi;
        double luas;
        double keliling;
        private void btnHasil_Click(object sender, EventArgs e)
        {
            sisi = double.Parse(txtSisi.Text);
            luas = sisi * sisi;
            keliling = 4 * sisi;
            txtLuas.Text = luas.ToString();
            txtKeliling.Text = keliling.ToString();
        }

        private void Persegi_Load(object sender, EventArgs e)
        {

        }

        private void btnKeluar_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
