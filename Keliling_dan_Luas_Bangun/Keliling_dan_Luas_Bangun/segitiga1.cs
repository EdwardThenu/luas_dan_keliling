﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Keliling_dan_Luas_Bangun
{
    public partial class segitiga1 : Form
    {
        public segitiga1()
        {
            InitializeComponent();
        }

        int a = 0;
        int b = 0;
        int c = 0;

        private void button1_Click(object sender, EventArgs e)
        {
            a = int.Parse(textBox1.Text);
            b = int.Parse(textBox2.Text);
            c = int.Parse(textBox3.Text);
            double i = (a + b + c) / 2D;
            richTextBox1.Text = Math.Round(Math.Sqrt(i * (i - a) * (i - b) * (i - c)), 1).ToString();
        }
    }
}
